package com.nextgenmarketting.utils;

/**
 * Created by ankush on 18-12-2016.
 */

public class Constants {

    // SharedPreferences
    public static final String NEXT_GEN_PREFERENCES = "nextgenmarket_preferences";
    public static final String USER_ID = "user_id";
    public static final String EMAIL = "email";
    public static final String USERNAME = "username";
    public static final String PHONE_NO = "phone";
    public static final String GENDER = "gender";
    public static final String CITY = "city";
    public static final String ABOUT_ME = "about_me";
    public static final String DESIGNATION = "designation";
    public static final String PLACE_OF_WORK = "place_of_work";
    public static final String COLLEGE = "college";
    public static final String SCHOOL = "school";
    public static final String REFERRAL_CODE = "referral_code";
    public static final String USER_IMAGE = "user_image";
    public static final String FCM_DEVICE_TOKEN = "fcm_device_token";
    public static final String NOTIFICATIONS_STATUS = "notifications_status";

    // For DatePicker and other Alert Dialog
    public static final class DialogConstant {
        public static final int ID_DATE_PICKER = 4000;
        public static final int ID_TIME_PICKER = 4001;
    }

    public static final class DateFromats {
        public static final String DATE_FORMAT = "yyyy-MM-dd";
        public static final String DISPLAY_DATE_FORMAT = "dd-MM-yyyy";   // For UI perspective
    }

    // Base Url
    public static final String BASE_URL = "http://www.nextgenmarketing.us";

    // No Photo Url
    public static final String NO_PHOTO_URL = "http://www.nextgenmarketing.us/userimages/no_photo.jpg";

    // Login User
    public static final String URL_USER_LOGIN = "http://www.nextgenmarketing.us/v1/user/Login";

    // Sign up User
    public static final String URL_USER_SIGNUP = "http://www.nextgenmarketing.us/v1/user/Register";

    // Forgot Password Url
    public static final String URL_FORGOT_PASSWORD = "http://www.nextgenmarketing.us/v1/user/ForgotPwd";

    // Upload Profile Image Url
    public static final String URL_UPLOAD_PROFILE_IMAGE = "http://www.nextgenmarketing.us/v1/photos/UpdateUserImage";

    // Search People Url
    public static final String URL_SEARCH_PEOPLE = "http://www.nextgenmarketing.us/v1/user/SearchUser?";

    // Search People Url
    public static final String URL_SEARCH_GROUP = "http://www.nextgenmarketing.us/v1/group/SearchGroup?";

    // Search People Url
    public static final String URL_SEND_FRIEND_REQUEST = "http://www.nextgenmarketing.us/v1/FriendRequest/SendFriendRequest";

    // New Friend Requests Url
    public static final String URL_NEW_FRIEND_REQUESTS = "http://www.nextgenmarketing.us/v1/FriendRequest/ReceivedFriendRequests?";

    // Accept Friend Request Url
    public static final String URL_ACCEPT_FRIEND_REQUEST = "http://www.nextgenmarketing.us/v1/FriendRequest/AcceptFriendRequest";

    // Reject Friend Request Url
    public static final String URL_REJECT_FRIEND_REQUEST = "http://www.nextgenmarketing.us/v1/FriendRequest/RejectFriendRequest";

    // Unfriend Friend Url
    public static final String URL_UNFRIEND_FRIEND = "http://www.nextgenmarketing.us/v1/Friend/Unfriend";

    // Share Text Url
    public static final String URL_SHARE_TEXT = "http://www.nextgenmarketing.us/v1/feeds/ShareText";

    // Share Image Url
    public static final String URL_SHARE_IMAGE = "http://www.nextgenmarketing.us/v1/photos/ShareImage";

    // Share Video Url
    public static final String URL_SHARE_VIDEO = "http://www.nextgenmarketing.us/v1/media/ShareVideo";

    // Get Feeds Url
    public static final String URL_GET_FEEDS = "http://www.nextgenmarketing.us/v1/feeds/FeedByUserID?";

    // Like Unlike Feed Url
    public static final String URL_LIKE_UNLIKE_FEED = "http://www.nextgenmarketing.us/v1/feeds/LikeUnlikeFeed";

    // Feed Comments Url
    public static final String URL_ADD_FEED_COMMENT = "http://www.nextgenmarketing.us/v1/feedcomment/Add";

    // Feed Comments Url
    public static final String URL_FEED_COMMENTS = "http://www.nextgenmarketing.us/v1/feedcomment/GetCommentsByFeedID?";

    // Friend PastEventModel Url
    public static final String URL_FRIEND_LIST = "http://www.nextgenmarketing.us/v1/Friend/GetFriends?";

    // Update User Profile PastEventModel Url
    public static final String URL_UPDATE_USER_PROFILE = "http://www.nextgenmarketing.us/v1/user/UpdateUserProfile";

    // Save Recent Call Url
    public static final String URL_SAVE_RECENT_CALL = "http://www.nextgenmarketing.us/v1/RecentCalls/SaveRecentCall";

    // Get Recent Calls Url
    public static final String URL_GET_RECENT_CALLS = "http://www.nextgenmarketing.us/v1/RecentCalls/GetMyRecentCalls?";

    // Notifications Url
    public static final String URL_GET_NOTIFICATIONS = "http://www.nextgenmarketing.us/v1/notifications/GetNotificationByUserID?";

    // Create Group Url
    public static final String URL_CREATE_GROUP = "http://www.nextgenmarketing.us/v1/group/AddGroup";

    // Groups Created By Me Url
    public static final String URL_GET_GROUPS_CREATED_BY_ME = "http://www.nextgenmarketing.us/v1/group/GetGroupsCreatedByMe?";

    // Groups Followed By Me Url
    public static final String URL_GET_GROUPS_FOLLOWED_BY_ME = "http://www.nextgenmarketing.us/v1/group/GetGroupsFollowedByMe?";

    // Get Feeds Url
    public static final String URL_GET_GROUP_FEEDS = "http://www.nextgenmarketing.us/v1/group/FeedByGroupID?";

    // Share Image In Group Url
    public static final String URL_SHARE_IMAGE_IN_GROUP = "http://www.nextgenmarketing.us/v1/photos/ShareImageInGroupFeed";

    // Share Video In Group Url
    public static final String URL_SHARE_VIDEO_IN_GROUP = "http://www.nextgenmarketing.us/v1/media/ShareGroupVideo";

    // Share Text In Group Url
    public static final String URL_SHARE_TEXT_IN_GROUP = "http://www.nextgenmarketing.us/v1/group/ShareGroupText";

    // Group Members PastEventModel Url
    public static final String URL_GROUP_MEMBERS_LIST = "http://www.nextgenmarketing.us/v1/group/GetGroupMembersWithoutMe?";

    // GET Friends Not In Group Url
    public static final String URL_GET_FRIENDS_NOT_IN_GROUP = "http://www.nextgenmarketing.us/v1/group/GetFriendsWhoAreNotInGroup?";

    // Add Group Members Url
    public static final String URL_ADD_GROUP_MEMBERS = "http://www.nextgenmarketing.us/v1/group/AddGroupMembers";

    // Delete Group Member Url
    public static final String URL_DELETE_GROUP_MEMBER = "http://www.nextgenmarketing.us/v1/group/ExitMember";

    // Delete Group Url
    public static final String URL_DELETE_GROUP = "http://www.nextgenmarketing.us/v1/group/DeleteGroup";

    // Notifications On/Off Url
    public static final String URL_NOTIFICATIONS_ON_OFF = "http://www.nextgenmarketing.us/v1/user/NotificationOnOff";

    // Change Password Url
    public static final String URL_CHANGE_PASSWORD = "http://www.nextgenmarketing.us/v1/user/ChangePassword";

    // Message Board Url
    public static final String URL_MESSAGE_BOARD = "http://www.nextgenmarketing.us/v1/admin/GetAdminMessage?";

    // Get Group Suggestions Url
    public static final String URL_GET_GROUP_SUGGESTIONS = "http://www.nextgenmarketing.us/v1/group/GetGroupSuggestions?";

    // Get Group Suggestions Url
    public static final String URL_GET_FRIEND_SUGGESTIONS = "http://www.nextgenmarketing.us/v1/Friend/GetFriendSuggestions?";

    // Get Another User Profile Url
    public static final String URL_GET_ANOTHER_USER_PROFILE = "http://www.nextgenmarketing.us/v1/user/GetAnotherUserByID?";

    // Save Profile View Url
    public static final String URL_SAVE_PROFILE_VIEW = "http://www.nextgenmarketing.us/v1/user/SaveProfileView";

    // Follow Unfollow Url
    public static final String URL_FOLLOW_UNFOLLOW = "http://www.nextgenmarketing.us/v1/Follow/FollowUnfollowUser";

    // Who Viewed My Profile Url
    public static final String URL_WHO_VIEWED_MY_PROFILE = "http://www.nextgenmarketing.us/v1/user/GetProfileViews?";

    // Hierarchy Chart Url
    public static final String URL_HIERARCHY_CHART = "http://www.nextgenmarketing.us/v1/user/GetHierarchy?";

    // Create Event Url
    public static final String URL_CREATE_EVENT = "http://www.nextgenmarketing.us/v1/events/CreateEvent";

    // Update Event Url
    public static final String URL_UPDATE_EVENT = "http://www.nextgenmarketing.us/v1/events/UpdateEvent";

    // Get Event By ID Url
    public static final String URL_GET_EVENT_BY_ID = "http://www.nextgenmarketing.us/v1/events/GetEventByID?";

    // Add Event Image Url
    public static final String URL_ADD_EVENT_IMAGE = "http://www.nextgenmarketing.us/v1/events/AddEventImage";

    // Delete Event Image Url
    public static final String URL_DELETE_EVENT_IMAGE = "http://www.nextgenmarketing.us/v1/events/DeleteEventImage";

    // Get Events Url
    public static final String URL_GET_EVENTS = "http://www.nextgenmarketing.us/v1/events/GetEvents?";

    // Get My Events Bookings Url
    public static final String URL_GET_MY_EVENTS_BOOKINGS = "http://www.nextgenmarketing.us/v1/events/GetMyBookings?";

    // Past Events Url
    public static final String URL_PAST_EVENTS = "http://www.nextgenmarketing.us/v1/events/GetPastEvents?";

    // Send Event Invitation Url
    public static final String URL_SEND_EVENT_INVITATION = "http://www.nextgenmarketing.us/v1/events/SendEventInvitation";

    // Buy Event Ticket Url
    public static final String URL_BUY_EVENT_TICKET = "http://www.nextgenmarketing.us/v1/events/BuyEventTicket";

    // Logout Url
    public static final String URL_LOGOUT = "http://www.nextgenmarketing.us/v1/user/Logout";
}
